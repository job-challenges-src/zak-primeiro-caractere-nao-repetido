from collections import Counter


def searching_challenge(sentences: str) -> str:
    first_not_repeate_caracter: str = None
    character_count = Counter(sentences)
    for character, count in character_count.items():
        if count == 1:
            first_not_repeate_caracter = character
            break
    return first_not_repeate_caracter