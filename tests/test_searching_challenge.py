from ..src.code import searching_challenge


def test_searching_challenge_sentence_1():
    sentence = 'abcdef'
    output = 'a'
    result = searching_challenge(sentence)
    assert result == output


def test_searching_challenge_sentence_2():
    sentence = 'hello world hi hey'
    output = 'w'
    result = searching_challenge(sentence)
    assert result == output
