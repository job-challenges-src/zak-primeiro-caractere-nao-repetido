## Empresa [Zak](https://www.linkedin.com/company/zakapp/)

### Vaga
#### Pessoa Desenvolvedora Backend (Python)


##### Faixa salarial 18k CLT

#### [Desafio](https://coderbyte.com/)


###### Criar um função que retorna o primeiro caractere não repetido em uma frase
###### Input: hello world hi hey
###### Output: w
